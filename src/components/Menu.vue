<template>
  <div 
    class="menu"
    :class="{'dark': darkMode}">
    <button
      class="menu__btn-previous"
      @click="onPreviousPageClick">
      Previous
    </button>

    <div class="menu__pages">
      <div
        v-for="pageIndex in pagesCount"
        v-show="pageIndex >= page - 2 && pageIndex <= page + 2 + (page <= 2 ? 3 - page : 0)"
        :key="pageIndex"
        class="menu__page"
        :class="{'active': pageIndex == page}"
        @click="onPageClick(pageIndex)">
        {{ pageIndex }}
      </div>
    </div>

    <button
      class="menu__btn-next"
      @click="onNextPageClick">
      Next
    </button>
    
    <input
      class="menu__input-page"
      type="text"
      :value="itemsPerPage"
      @input="onItemsPerPageChange">
  </div>
</template>

<script>
export default {
  name: 'Menu',
  props: {
    page: {
      type: Number,
      default: 1,
    },
    itemsPerPage: {
      type: Number,
      default: 12,
    },
    pagesCount: {
      type: Number,
      default: 1,
    },
    darkMode: Boolean,
  },
  mounted() {
    window.addEventListener('keydown', e => {
      if (e.keyCode == 37) { // left arrow
        this.onPreviousPageClick();
      } else if (e.keyCode == 39) { // right arrow
        this.onNextPageClick();
      }
    }, false); // false in order to run after Modal's keydown event
  },
  methods: {
    onItemsPerPageChange(e) {
      let value = Math.max(1, parseInt(e.target.value));
      value = Math.min(100, parseInt(value));
      if (value) {
        let debounceTimeout = window.changeItemsPerPageTimeout;
        clearTimeout(debounceTimeout);
        window.changeItemsPerPageTimeout = setTimeout(() => {
          this.$emit('itemsPerPageChange', value);
        }, 500);
      }
    },
    onNextPageClick() {
      let nextPage = this.page + 1;
      if (nextPage <= this.pagesCount) {
        this.$emit('pageChange', nextPage);
      }
    },
    onPreviousPageClick() {
      let previousPage = this.page - 1;
      if (previousPage > 0) {
        this.$emit('pageChange', previousPage);
      }
    },
    onPageClick(page) {
      this.$emit('pageChange', page);
    }
  },
}
</script>

<style lang="scss" scoped>
.menu {
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  user-select: none;
  padding: 10px 0;
  width: 400px;

  @media screen and (max-width: 400px) {
    width: auto;
  }

  @media screen and (max-width: 768px) { // tablet devices
    position: fixed;
    bottom: 0;
    background: #FFF;
    width: 100%;
  }

  .menu__btn-previous, .menu__btn-next {
    width: 90px;
    height: 30px;
    background: transparent;
    border: 1px solid #333;
    margin: 1px;
    padding: 2px 5px;
    cursor: pointer;
    outline: none;

    &:hover {
      background: #333;
      color: #efefef;
    }
  }

  .menu__pages {
    display: inline-flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: 200px;

    .menu__page {
      display: inline-flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      flex-basis: 20%;
      height: calc(30px - 2px);
      font-size: 13px;
      margin: 0 1px;
      border: 1px solid #333;
      cursor: pointer;

      &.active {
        background: #333;
        border-color: #333;
        color: #EEE;
      }
    }
  }

  .menu__input-page {
    padding: 0;
    outline: none;
    width: 28px;
    height: 28px;
    font-size: 15px;
    border: 1px solid #333;
    text-align: center;
  }

  &.dark {
    .menu__btn-previous, .menu__btn-next {
      border-color: #D9D9D9;
      color: #EEE;

      &:hover {
        background: #EFEFEF;
        border-color: #D9D9D9;
        color: #333;
      }
    }

    .menu__page {
      border-color: #D9D9D9;

      &.active {
        background: #EFEFEF;
        border-color: #D9D9D9;
        color: #333;
      }
    }

    .menu__input-page {
      border-color: #EEE;
    }

    @media screen and (max-width: 768px) { // tablet devices
      background: #111;
    }
  }
}
</style>
